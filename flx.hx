# Ultima 9 .FLX file format
# Based on work by John Larcombe aka TumbleWeed Dragon
#
# The FLX file's used in Ultima 9 are very similar to those used in Ultima's 7 and 8, and follow the same purpose:
# to group individual files of a similar type into one parent file, very much like a zip file except there is no compressing, and no storage of filenames.
# 
# A general FLX file is made up of three things : A header, a file information chunk, and a file data chunk.
# 
# The header basically contains room for a comment, the total filesize, number of sub-files, 
# 
# File information chunk contains the offsets and sizes of the individual sub-files.
# 
# File data chunk contains the actual data for the sub-files.

<=flx_header>
S{comment}*80
L{nr_subfiles}
L{unknown_0} # 2?
L{filesize}
L{filesize_2}
..*8
L{unknown_1} # 1?
..*32
</flx_header>

<=subfile_table>
<=subfile_info>
L{offset}
L{size}
</subfile_info>
<subfile_info sf_{i}/>*{nr_subfiles}
</subfile_table>

<=subfile>
# replace me with actual subfile format
{offset}: # start
+{size}: # end
</subfile>

<=flx>
<flx_header />
<subfile_table />
<subfile sf_{i}/>*{nr_subfiles}
</flx>
